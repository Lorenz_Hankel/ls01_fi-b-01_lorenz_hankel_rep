
public class Fakult�t {

	public static void main(String[] args) {
		//Ich definiere die erste Zeile bis zum ersten = Zeichen
		System.out.printf("0! %-4s", "  =");
		//Ich f�ge einen 19er string an
		System.out.printf("%-18s", "");
		//Ich f�ge das 2te Gleichheitszeichen und den letzten rechtsb�ndigen string ein
		System.out.printf("= %3s", 1);
		//aus irgendeinen Grund sind teils mehrere Zeichen zu viel, trotz Definition
		//Ich beginne die 2te Zeile und kopiere einfach immer die oberen Programmzeilen
		System.out.printf("\n1! %-4s", "  =");
		System.out.printf("%-18s", "1");
		System.out.printf("= %3s", 1);
		//3te Zeile
		System.out.printf("\n2! %-4s", "  =");
		System.out.printf("%-18s", "1 * 2");
		System.out.printf("= %3s", 1);
		//4te Zeile
		System.out.printf("\n3! %-4s", "  =");
		System.out.printf("%-18s", "1 * 2 * 3");
		System.out.printf("= %3s", 1);
		//5te Zeile
		System.out.printf("\n4! %-4s", "  =");
		System.out.printf("%-18s", "1 * 2 * 3 * 4");
		System.out.printf("= %3s", 1);
		//6te Zeile
		System.out.printf("\n5! %-4s", "  =");
		System.out.printf("%-18s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("= %3s", 1);
	}

}
