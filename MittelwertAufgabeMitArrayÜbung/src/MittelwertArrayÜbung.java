import java.util.*;

public class MittelwertArray�bung {

	public static void main(String[] args) {
		
		int zahl1 = 0;
		double m = 0;
		
		programmhinweis();
		// (E) "Eingabe"
		zahl1 = eingabe("\nMit wie vielen Zahlen soll das Array belegt werden?\n");
		
		
		// (V) Verarbeitung
		int[] zahlenArray = arrayAusgabe(zahl1);
		m = berechneMittelwert(zahlenArray);
		

		// (A) Ausgabe
		ausgabe(m);

	}
	// Methode Programmhinweis

	public static void programmhinweis() {
		System.out.println("Dieses Programm berechnet den Mittelwert eines Arrays, dessen L�nge in der folgenden Eingabe erfasst wird. \nDie Ganzzahlen, mit denen das Array gef�llt wird werden zuf�llig aus 0 bis 99 ausgew�hlt.");
	}

	// Methode Eingabe

	public static int eingabe(String text) {
		Scanner scan = new Scanner(System.in);
		System.out.print(text);
		int zahl = scan.nextInt();
		return zahl;
	}

	public static int[] arrayAusgabe(int x1) {
		int[] zahlenArray = new int[x1];
		Random zufall = new Random();
		for (int i=0; i < zahlenArray.length; i++) {
			zahlenArray[i] = zufall.nextInt(99);
			System.out.println("Index: " + i + " Wert: " + zahlenArray[i]);
		}
		return zahlenArray;
	}
	// Methode Verarbeitung
	public static double berechneMittelwert(int[] x1) {
		double mittelwert;
		int summe = 0;
		for (int i=0; i<x1.length; i++) {
			summe += x1[i];
		}
		mittelwert = summe/x1.length-1;
		return mittelwert;
	}


	// Methode Ausgabe
	public static void ausgabe(double x) {
		System.out.println("Der Mittelwert von dem Array ist " + x);
	}
}
