
public class Aufgabe3 {

	public static void main(String[] args) {
		/*
		 * Ich lege die grobe Formatierung der Tabelle fest, 12 Zeichen String linksb�ndig auf der linken Seite
		 *1 Zeichen String mit Pipe in der Mitte als Trennzeichen
		 *und abschliessend 9 Zeichen String rechtsb�ndig auf der rechten Seite
		 */
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("%1s", "|");
		System.out.printf("%9s", "Celsius");
		/*
		 * Als Abschluss der oberen Zeile f�ge ich jetzt noch eine Zeile von - Zeichen mit 23er Zeichenkette ein.
		 */
		System.out.printf("\n%23s", "-----------------------");
		/*
		 * Jetzt f�hre ich einfach nur diese Formatierung weiter fort und f�lle meine Werte aus der vorgegebenen Tabelle ein.
		 * Zus�tzlich muss ich noch die Celsiuswerte per Formatierung auf 2 Nachkommastellen runden lassen.
		 * Es ist auch zu beachten, dass nur Strings in Anf�hrungszeichen gesetzt werden k�nnen. Bei Float ergibt das Fehler.
		 */
		//Erste Zeile der Werte, die Fahrenheitwerte m�ssen als Double definiert  werden, da FLOAT Fehler schmeisst.
		System.out.printf("\n%-12s", "-20.0");
		System.out.printf("%1s", "|");
		System.out.printf("%9.2f", -28.8889);
		
		System.out.printf("\n%-12s", "-10.0");
		System.out.printf("%1s", "|");
		System.out.printf("%9.2f", -23.3333);
		
		System.out.printf("\n%-12s" , "+0.0");
		System.out.printf("%1s", "|");
		System.out.printf("%9.2f", -17.7778);
		
		System.out.printf("\n%-12s", "+20.0");
		System.out.printf("%1s", "|");
		System.out.printf("%9.2f", -6.6667);
		
		System.out.printf("\n%-12s", "+30.0");
		System.out.printf("%1s", "|");
		System.out.printf("%9.2f", -1.1111);
	}

}
