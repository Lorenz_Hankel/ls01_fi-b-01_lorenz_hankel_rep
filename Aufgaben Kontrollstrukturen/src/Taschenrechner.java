import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		//Aufgabe Taschenrechner aus Kontrollstrukturen
		//Ich initialisiere und deklariere meine Variablen und den Scanner
		
		double zahl1 = 0.0;
		double zahl2 = 0.0;
		char operator = ' ';
		double ergebnis = 0.0;
		Scanner eingabe = new Scanner(System.in);
		
		//Ich fordere zur Eingabe auf und gebe den Programmhinweis
		
		System.out.println("Dieses Programm ist ein simpler Taschenrechner. Zwei eingegebene Zahlen werden miteinander verrechnet mit dem eingegebenen mathematischen Operator");
		
		System.out.println("Bitte geben Sie die erste Zahl ein.");
		
		zahl1 = eingabe.nextDouble();
		
		System.out.println("Bitte geben Sie die zweite Zahl ein.");
		
		zahl2 = eingabe.nextDouble();
		
		System.out.println("Bitte geben Sie den gew�nschten mathematischen Operator ein.");
		
		operator = eingabe.next().charAt(0);
		
		//Ich erstelle ein Switch Case und fange an die Operatoren auszuwerten und zu verrechnen (Verarbeitung)
		//au�erdem rufe ich meine unten erstellte Methode zur Ergebnisausgabe immer auf.
		switch (operator) {
			case  '+' :
				ergebnis = (zahl1 + zahl2);
				ergebnisausgabe(ergebnis);
			break;
			
			case '-' :
				ergebnis = (zahl1 - zahl2);
				ergebnisausgabe(ergebnis);
			break;
			
			case '*':
				ergebnis = (zahl1 * zahl2);
				ergebnisausgabe(ergebnis);
			break;
			
			case '/' :
				ergebnis = (zahl1 / zahl2);
				ergebnisausgabe(ergebnis);
			break;
			
			default:
			System.out.println("Ung�ltige Eingabe/n!");
			break;
		
		}

	}
	//Ich erstelle eine Methode welche Ergebnisse ausgibt, um Wiederholung zu vermeiden.
public static void ergebnisausgabe(double ergebnis) {
	System.out.println("Das Ergebnis ist: " + ergebnis + " .");
}
}
