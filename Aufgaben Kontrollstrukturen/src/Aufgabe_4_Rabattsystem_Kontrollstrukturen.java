import java.util.Scanner;
import java.lang.Math;
public class Aufgabe_4_Rabattsystem_Kontrollstrukturen {

	public static void main(String[] args) {
		
//		Aufgabe 4: Rabattsystem Der Hardware-Gro�h�ndler f�hrt ein Rabattsystem ein: 
//			Liegt der Bestellwert zwischen 0 und 100 �, erh�lt der Kunde einen Rabatt von 10 %. 
//			Liegt der Bestellwert h�her, aber insgesamt nicht �ber 500 �, betr�gt der Rabatt 15 %, 
//			in allen anderen F�llen betr�gt der Rabatt 20 %. Nach Eingabe des Bestellwertes soll der 
//			erm��igte Bestellwert (incl. MwSt.) berechnet und ausgegeben werden.

		//Zuerst initialisiere und deklariere ich alle n�tigen Variablen und den Scanner
		
		String artikel = "";
		double artikelpreis = 0.0;
		byte artikelmenge = 0;
		double gesamtpreis = 0.0;
		double nachAbzuegen = 0.0;
		double mwst = 0.16;
		Scanner eingabe = new Scanner(System.in);
		
		//Jetzt f�hre ich die Abfragen und die Eingabe durch.
		
		System.out.println("Bitte geben Sie den zu kaufenden Artikelnamen ein und best�tigen Sie mit Return.");
		artikel = eingabe.next();
		
		
		System.out.println("Bitte geben Sie die Anzahl des zu kaufenden Artikels " + artikel + " ein  und best�tigen Sie mit Return.");
		artikelmenge = eingabe.nextByte();
		
		System.out.println("Bitte geben Sie nun den Artikelpreis ein und best�tigen Sie mit Return.");
		artikelpreis = eingabe.nextDouble();
		
		
		
		//Jetzt implementiere ich die Verarbeitung.
		
		
		gesamtpreis = artikelpreis * artikelmenge;
		
		//vorab Ausgabe von Preisen vor den Abz�gen von Rabatten.
		
		System.out.println("Der Gesamtpreis vor den Abz�gen betr�gt: " + gesamtpreis + " �." );
		
		if (gesamtpreis <= 100.00) {
			System.out.println("Sie erhalten einen Rabatt von 10% auf Ihren Gesamtpreis.");
			nachAbzuegen = (gesamtpreis - gesamtpreis * 0.10);
			System.out.println("Der Preis nach den Abz�gen betr�gt : " + nachAbzuegen + " �.");
			System.out.println("Die zzgl. Mwst. betr�gt: " + (nachAbzuegen * mwst) + " �.");
			System.out.println("Sie zahlen insgesamt: " + (nachAbzuegen + mwst * nachAbzuegen) + " �.");
			System.out.println("Sie haben sich f�r " + artikelmenge + " * " + artikel + " entschieden.");
		}
		else if (gesamtpreis < 500.00 && gesamtpreis > 100.00) {
			System.out.println("Sie erhalten einen Rabatt von 15% auf Ihren Gesamtpreis.");
			nachAbzuegen = (gesamtpreis - gesamtpreis * 0.15);
			System.out.println("Der Preis nach den Abz�gen betr�gt : " + nachAbzuegen + " �.");
			System.out.println("Sie haben sich f�r " + artikelmenge + " * " + artikel + " entschieden.");
			System.out.println("Sie zahlen insgesamt: " + (nachAbzuegen + mwst * nachAbzuegen) + " �.");
			System.out.println("Sie haben sich f�r " + artikelmenge + " * " + artikel + " entschieden.");
		}
		else {
			System.out.println("Sie erhalten einen Rabatt von 20% auf Ihren Gesamtpreis, da Sie mehr als 500,00� vor den Abz�gen bezahlen w�rden.");
			nachAbzuegen = (gesamtpreis - gesamtpreis * 0.20);
			System.out.println("Der Preis nach den Abz�gen betr�gt : " + nachAbzuegen + " �.");
			System.out.println("Sie haben sich f�r " + artikelmenge + " * " + artikel + " entschieden.");
			System.out.println("Sie zahlen insgesamt: " + (nachAbzuegen + mwst * nachAbzuegen) + " �.");
			System.out.println("Sie haben sich f�r " + artikelmenge + " * " + artikel + " entschieden.");
		}
		
		
	}

}
