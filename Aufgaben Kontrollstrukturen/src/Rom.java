import java.util.Scanner;
public class Rom {

	public static void main(String[] args) {
		//Aufgabe Rom aus Kontrollstrukturen AB
		
		//Initialiserung und Deklaration von relevanten Variablen und dem Scanner
		
		char ziffer = ' ';
		int ganzzahl = 0;
		Scanner eingabe = new Scanner(System.in);
		
		//Programmhinweis
		
		System.out.println("Bitte geben Sie eine R�mische Ziffer ein und best�tigen Sie mit Return.");
		ziffer = eingabe.next().charAt(0);
		
		//Switch Cases erstellen f�r alle vorgegebenen F�lle aus der Ausgabe, und jedes Mal eine Ausgabe machen.
		
		switch (ziffer){
			case 'I':
			ganzzahl = 1;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
			
			case 'V':
			ganzzahl = 5;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
		
			case 'X':
			ganzzahl = 10;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
			
			case 'L':
			ganzzahl = 50;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
		
			case 'C':
			ganzzahl = 100;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
		
			case 'D':
			ganzzahl = 500;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
		
			case 'M':
			ganzzahl = 1000;
			System.out.println("Die entsprechende Dezimalzahl ist: " + ganzzahl);
			break;
			
			//Ausnahme Case f�r ung�ltige Eingaben.
			
			default:
				System.out.println("ung�ltige Eingabe!");
				break;
		
		
		}
		
		
		

	}

}
