//Viel Spa� beim Lesen des Quelltextes ;) beim st�ndigen Umstrukturieren zwischen LS01-11 bis LS01-13 und LS01-09
//Ich importiere den Scanner zuerst
import java.lang.reflect.Array;
import java.util.Scanner;


class Fahrkartenautomat
	{


    //Main Methode erstellt 
	//Hier wird die grobe Reihenfolge der einzelnen Funktionen festgelegt.
	
	public static void main(String[] args)
    
    {
    	int fehler = 0; //Bedingung f�r die Schleife der Main Methode aus Aufgabe 6
		do {
		
    	//Ich initialisiere meine Variablen die ich in der Main Methode brauche
      
       double zuZahlenderBetrag = 0.0; 
       double eingezahlterGesamtbetrag = 0.0;
       
   		//Programmhinweis
       
       System.out.println("Dieses Programm berechnet zu zahlende Betr�ge f�r Ticketbestellungen.");
       
       //Eingabe
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       //Verarbeitung
       //Bezahlroutine starten
       
       eingezahlterGesamtbetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag);

       
       //Ticketausgabe starten
       
       fahrkartenAusgeben();
       
       //R�ckgeldausgaberoutine starten
       
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
       //Trennlinie zwischen den Bestellvorg�ngen als Abschluss
       System.out.println("____________________________________________________");
       
//ENDE MAIN METHODE
       
       //Ich setze die Bedingung f�r meine fu�gesteuerte Main Schleife
		}while(fehler == 0);
       
    }
	
	
	
	
	//Methode f�r Die Erfassung von Eingaben erstellt
	
	
	public static double eingabenErfassen() { //Methode wird seit 07++ nicht mehr zum Bezahlen genutzt, damit ich auf korrekten M�nzen pr�fen kann.
		Scanner eingabe = new Scanner(System.in);
		double zahl = eingabe.nextDouble();
		//Arbeitsauftrag LS01-11 (Aufgabe 05 bzw. 05++) Es sollen nur Anzahl von 1-10 zugelassen werden.
		//if (zahl < 0) {
			//zahl = 1;
			//System.out.println("Ung�ltige Eingabe! Ihre Eingabe wurde auf 1 stattdessen gesetzt. G�ltige Eingaben sind positive Werte.");
		//}
			
		return zahl;
	}
		public static double bezahlomat() {
			Scanner eingabe = new Scanner(System.in);
			double zahl = 0;
			zahl = eingabe.nextDouble();
			boolean korrekt = false;
			while(korrekt == false) {
				if(zahl == 0.01 || zahl == 0.02 || zahl == 0.02 || zahl == 0.05 || zahl == 0.1 || zahl == 0.2 || zahl == 0.5 || zahl == 1.0 || zahl == 2.0) { //If Abfrage um zu pr�fen ob korrekte M�nzen genutzt wurden.
					korrekt = true;
				}
					else {
						korrekt = false;
						System.out.println("\nung�ltige M�nze eingeworfen, bitte eine g�ltige M�nze einwerfen!\n");
						zahl = eingabe.nextDouble();
					}
			
				}
			return zahl;
			}
			
		
	
	//Methode f�r Gesamtpreis Berechnung erstellt
	public static double fahrkartenbestellungErfassen() {
		//Ich initialisiere und deklariere einen neuen Scanner um ihn lokal zu nutzen.
		Scanner eingabe = new Scanner(System.in);
		//Schleife f�r Erfassung anzahltickets
		double anzahltickets = 0;

		do{
			System.out.print("Anzahl der Tickets: ");
		//anzahltickets = eingabenErfassen(); //alte Methode um Scanner zu nutzen
		anzahltickets = eingabe.nextDouble();
		if (anzahltickets > 10 | anzahltickets < 1) {
			System.out.println("Ung�ltige Eingabe! Bitte geben Sie einen g�ltigen Wert zwischen 1 und 10 ein.");
			 }
		
		}while(anzahltickets < 1 | anzahltickets > 10);
		
		System.out.println("Bitte W�hlen Sie die Nummer von einen der folgenden Tickettypen: ");
		//System.out.printf("1) BERLIN AB:  2,90� \n2) BERLIN BC:  3,30� \n3) BERLIN ABC: 3,60� \n"); //Veraltete Ausgabe f�r Teilaufgabe 06
		
		//double ticketpreis = 0.0; //alte Variable f�r Teilaufgabe 6
		int ticketTyp = 0; //veraltete Variable dank Aufgabe 07
		
		//Aufgabe Fahrkartenautomat - 07 Erstellen von zwei eindimensionalen Arrays f�r die Zuordnung von TicketTyp und TicketPreis
		String[] ticketName = new String[10]; 
		ticketName[0] = "1) Einzelfahrschein Berlin AB";
		ticketName[1] = "2) Einezlfahrschein Berlin BC";
		ticketName[2] = "3) Einzelfahrschein Berlin ABC";
		ticketName[3] = "4) Kurzstrecke";
		ticketName[4] = "5) Tageskarte Berlin AB";
		ticketName[5] = "6) Tageskarte Berlin BC";
		ticketName[6] = "7) Tageskarte Berlin ABC";
		ticketName[7] = "8) Kleingruppen-Tageskarte Berlin AB";
		ticketName[8] = "9) Kleingruppen-Tageskarte Berlin BC";
		ticketName[9] = "10) Kleingruppen-Tageskarte Berlin ABC";
		
		//Array erstellen f�r die Preise der obigen Tickets
		double[] ticketpreis = new double[10];
		ticketpreis[0] = 2.90;
		ticketpreis[1] = 3.30;
		ticketpreis[2] = 3.60;
		ticketpreis[3] = 1.90;
		ticketpreis[4] = 8.60;
		ticketpreis[5] = 9.00;
		ticketpreis[6] = 9.60;
		ticketpreis[7] = 23.50;
		ticketpreis[8] = 24.30;
		ticketpreis[9] = 24.90;
		
		//Ausgabe der m�glichen Tickets
		for (int i = 0; i < ticketName.length; i++){
			System.out.print(ticketName[i] + "  " + ticketpreis[i] + " \n");
		}
		
		//Zuweisung von der Eingabe des Nutzers f�r die Auswahl des Tickets. Es wird direkt -1 gerechnet um die Stelle korrekt dem Array zuzuweisen.
		//ticketTyp = (eingabe.nextInt()-1);
		
		//Sicherheitsfunktion, dass nur korrekte Arrayfelder ausgelesen werden. Erzeugt Schleife bis korrekte Eingabe erfolgt.
		ticketTyp = -1;
		while(ticketTyp < 0 | ticketTyp > 9) {
			
			
		ticketTyp = (eingabe.nextInt()-1);
			if (ticketTyp < 0 | ticketTyp > 9) {
				System.out.println("Ung�ltige Eingabe f�r den Ticket Typ! Bitte eine Nummer zwischen 1 und 10 eingeben!\nG�ltige Tickets sind wie folgt: ");
				for (int i = 0; i < ticketName.length; i++){
					System.out.print(ticketName[i] + "  " + ticketpreis[i] + " \n");
				}
			}
		
		}
		
		//Folgender Quelltext au�erkraftgesetzt dank der Aufgabe 07
		//Switch Cases erstellt f�r die Teilaufgabe Fahrkartenautomat - 06.03 mit den aktuell g�ltigen VBB Preisen f�r Einzelfahrkarten.
		//switch(ticketTyp) {
			//case 1:
			//ticketpreis = 2.90;
			//break;
			//case 2:
				//ticketpreis = 3.30;
			//break;
			//case 3:
				//ticketpreis = 3.60;
			//break;
			//default:
				//System.out.println("Fehler bei SwitchCase Ticket Typ Wahl!");
			
		
		//}
		
		//Initialisierung und Deklaration vom zu zahlenden Betrag, ausgelesen wird direkt aus dem double Array ticketpreis
		double zuZahlenderBetrag = (anzahltickets * ticketpreis[ticketTyp]);
		//System.out.println("Ausgelesener Wert f�r ergebnis: " + zuZahlenderBetrag); //Debug Zeile f�r Werte
		
		//Ausgabe des Gesamtbetrages zur Selbstkontrolle.
		System.out.println(" ");
		System.out.println("F�lliger Gesamtbetrag: " + zuZahlenderBetrag + " EURO.");
		System.out.println(" ");
		return zuZahlenderBetrag;
	}
	
	
	
	
	//Methode f�r den Bezahlvorgang erstellt
	public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) 
	{
		// Geldeinwurf
	       // -----------
	       double eingeworfeneM�nze = 0.0;
	       	//while Schleife als Abfrage ob der zuZahlenderBetrag erreicht wurde.
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   
	    	   System.out.printf("Noch zu zahlen: %.2f\n " ,  (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = bezahlomat(); //Ich nutze hier eine Funktion zum Einlesen von Eingaben, anstatt den Scanner in jeder Funktion aufzurufen.
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	       //System.out.println("ausgelesener Wert f�r den return: " + (eingezahlterGesamtbetrag - zuZahlenderBetrag)); //Debug Zeile f�r den Return
		return eingezahlterGesamtbetrag;
		}
	
	
	//Methode fahrkartenAusgeben erstellt
//	Die Methode Fahrscheinausgabe fasst den bisherigen Quellcode f�r die Ausgabe als Methode zusammen und wird vor der R�ckgeldausgabe einmalig aufgerufen
    public static void fahrkartenAusgeben() {
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(150);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
    //Methode rueckgeldAusgeben erstellt
//    Diese Methode fasst die R�ckgeldausgabe zusammen. Sie nimmt die Variable eingezahlter Gesamtbetrag und den errechneten Wert f�r 
//    die Variable zuZahlenderBetrag, errechnet das n�tige Wechselgeld
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        //alte Formel auskommentiert und durch Methode ersetzt.
        double r�ckgabebetrag = 0.0;
        double nullwert = 0.0;
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        nullwert = 0.00;
//        System.out.println("Ausgelesener Wert r�ckgabebetrag: " + r�ckgabebetrag); // Debugzeile f�r r�ckgabebetrag
        if(r�ckgabebetrag >  nullwert)
        {
     	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.04)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
            while(r�ckgabebetrag >= 0.015)// 2 CENT-M�nzen
            {
         	  System.out.println("2 CENT");
  	          r�ckgabebetrag -= 0.02;
            }
            while(r�ckgabebetrag >= 0.005)// 1 CENT-M�nzen
            {
         	  System.out.println("1 CENT");
  	          r�ckgabebetrag -= 0.01;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}